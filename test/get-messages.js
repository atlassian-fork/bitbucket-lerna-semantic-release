import { expect } from 'chai';
import getMessages from '../src/get-messages';

describe('getMessages', () => {
  it('collects the .messages', () => {
    expect(getMessages([{ message: 1 }, { message: 2 }, { message: 3 }])).to.deep.equal([1, 2, 3]);
  });
});
