import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

chai.should();
chai.use(chaiAsPromised);

global.window = {};
import getCommits from '../src/get-commits'; // eslint-disable-line import/imports-first

function stubRequest(responses) {
  window = {};
  window.AP = {
    require: (req, cb) => {
      if (req === 'request') {
        cb(({
          url,
          success,
          error,
        }) => {
          const response = responses[url];
          if (response) {
            success(response);
          } else {
            error('no response for url');
          }
        });
      }
    },
  };
}

function makeUrl(user, repo, pullrequestid, page) {
  return `/2.0/repositories/${user}/${repo}/pullrequests/${pullrequestid}/commits?page=${page}`;
}

function restoreRequest() {
  global.window = {};
  global.window.AP = {};
}

describe('getCommits', () => {
  afterEach(() => {
    restoreRequest();
  });

  it('collects the right number of values', () => {
    stubRequest({
      [makeUrl('user', 'repo', 1, '0')]: { values: [0], next: true },
      [makeUrl('user', 'repo', 1, '1')]: { values: [1], next: true },
      [makeUrl('user', 'repo', 1, '2')]: { values: [2], next: false },
    });
    return getCommits('user', 'repo', 1).should.eventually.deep.equal([0, 1, 2]);
  });
  it('stops when next=false', () => {
    stubRequest({
      [makeUrl('user', 'repo', 1, '0')]: { values: [0], next: true },
      [makeUrl('user', 'repo', 1, '1')]: { values: [1], next: false },
      [makeUrl('user', 'repo', 1, '2')]: { values: [2], next: true },
    });
    return getCommits('user', 'repo', 1).should.eventually.deep.equal([0, 1]);
  });
  it('returns no commits when none exist', () => {
    stubRequest({});
    return getCommits('user', 'repo', 1).should.be.rejectedWith('failed due to no response for url');
  });
});
