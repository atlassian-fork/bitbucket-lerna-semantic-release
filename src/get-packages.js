import { findAffectsLine } from 'lerna-semantic-release-analyze-commits';
import getMessages from './get-messages';

export default function getPackages(commits) {
  const messages = getMessages(commits);
  return messages
    .map(message => findAffectsLine({ message }))
    .filter(affectsLine => !!affectsLine)
    .map(affectsLine => affectsLine.split('affects: ')[1])
    .map(packages => packages.split(',').map(s => s.trim()))
    .reduce((packageList1, packageList2) => packageList1.concat(packageList2), [])
    .map(pkg => pkg.substring(pkg.lastIndexOf('@')))
    .reduce((uniquePackages, pkg) =>
        (uniquePackages.indexOf(pkg) === -1 ? uniquePackages.concat([pkg]) : uniquePackages),
      []);
}
