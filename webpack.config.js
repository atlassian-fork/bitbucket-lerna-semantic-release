const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    app: [
      './src/index.js',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
  },
  externals: {
    winston: 'require("winston")',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
        },
      },
      {
        test: /node_modules\/conventional-changelog-writer/,
        loader: 'babel-loader',
        query: {
          plugins: ['static-fs'],
        },
      },
      {
        test: /\.hbs$/,
        loader: 'raw-loader',
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.less$/,
        loader: 'style!css!less',
      },
    ],
  },
  node: {
    fs: 'empty',
    child_process: 'empty',
  },
  plugins: [
    new webpack.IgnorePlugin(/^\.\/test\/.*$/, /conventional-changelog-angular$/),
    new webpack.optimize.UglifyJsPlugin(),
  ],
};
